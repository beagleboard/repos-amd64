Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dt-schema
Upstream-Contact: Devicetree Specification <devicetree-spec@vger.kernel.org>
Source: https://github.com/devicetree-org/dt-schema

Files: *
Copyright:
 2018-2019 Linaro Ltd.
 2018-2022 Arm Ltd.
License: BSD-2-clause

Files:
 dtschema/schemas/graph.yaml
 dtschema/schemas/iio/iio-consumer.yaml
 dtschema/schemas/iio/iio.yaml
 dtschema/schemas/pci/pci-bus.yaml
Copyright:
 2018-2019 Linaro Ltd.
 2018-2021 Arm Ltd.
License: BSD-2-clause or GPL-2

Files: dtschema/dtschema/schemas/options*
Copyright: 2021 Google LLC
License: BSD-2-clause or GPL-2

Files: dtschema/schemas/chosen.yaml
Copyright: 2014,2018 Linaro Ltd.
Comment: Descriptions based on chosen.txt from Linux kernel
 Copyright 2017 Kees Cook<keescook@chromium.org>
 Copyright 2017 Jonathan Neuschäfer
 Copyright ARM Ltd 2017
 Copyright 2015 Freescale Semiconductor, Inc.
License: BSD-2-clause or GPL-2

Files: dtschema/schemas/clock/clock.yaml
Copyright: 2018 Linaro Ltd.
           2012 Secret Lab Technologies Ltd.
Comment: Description text is from the Devicetree Specification at
   https://www.devicetree.org/specifications/
 which is
 Copyright 2008,2011 Power.org, Inc.
 Copyright 2008,2011 Freescale Semiconductor, Inc.
 Copyright 2008,2011 International Business Machines Corporation
 Copyright 2016,2017 Linaro,Ltd.
 Copyright 2016,2017 Arm,Ltd.
License: BSD-2-clause

Files: dtschema/schemas/cpus.yaml
Copyright: (C) 2018-2019 SiFive, Inc.
Comment: Description text is from the Devicetree Specification at
   https://www.devicetree.org/specifications/
 which is
 Copyright 2008,2011 Power.org, Inc.
 Copyright 2008,2011 Freescale Semiconductor, Inc.
 Copyright 2008,2011 International Business Machines Corporation
 Copyright 2016,2017 Linaro,Ltd.
 Copyright 2016,2017 Arm,Ltd.
License: BSD-2-clause or GPL-2

Files: dtschema/schemas/i2c/i2c-controller.yaml
Copyright:
 2018 Linaro Ltd.
 2015,2020 Wolfram Sang <wsa@sang-engineering.com>
 2022 Arm Ltd.
License: BSD-2-clause

Files: test/conditionals-*.dts
       test/schemas/conditionals-*.yaml
Copyright: 2019 Maxime Ripard
License: BSD-2-clause

Files: debian/*
Copyright: 2022-2024 Agathe Porte <gagath@debian.org>
License: BSD-2-clause

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
