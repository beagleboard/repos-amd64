dt-schema (2023.11-3bbbio0~trixie+20240827) trixie; urgency=low

  * Rebuild dt-schema_2023.11-3 for repos.rcn-ee.com

 -- Robert Nelson <robertcnelson@gmail.com>  Tue, 27 Aug 2024 16:15:08 -0500

dt-schema (2023.11-3) unstable; urgency=medium

  * Revert "autopkgtest: Replace py3versions -d with -s"
  * Explain in autopkgtest why we test only default Python

 -- Agathe Porte <gagath@debian.org>  Sun, 28 Jan 2024 21:56:05 +0100

dt-schema (2023.11-2) unstable; urgency=medium

  * Source-only upload

 -- Agathe Porte <gagath@debian.org>  Wed, 24 Jan 2024 10:58:30 +0100

dt-schema (2023.11-1) unstable; urgency=medium

  * New upstream version 2023.11 (Closes: #1058930)
  * Replace dh-python B-D with pybuild-plugin-pyproject
  * autopkgtest: Replace py3versions -d with -s
  * Update maintainer address
  * Introduce py3dist-overrides for python3-libfdt
  * Update Standards-Version to 4.6.2 (no change)
  * Fix SyntaxWarning: invalid escape sequence

 -- Agathe Porte <gagath@debian.org>  Tue, 23 Jan 2024 19:51:25 +0100

dt-schema (2022.08.2-5) unstable; urgency=medium

  * Team upload
  * autopkgtests: py3versions: use -d instead of -s (Closes: #1024910)

 -- Bastian Germann <bage@debian.org>  Fri, 16 Dec 2022 02:34:50 +0100

dt-schema (2022.08.2-4) unstable; urgency=medium

  * autopkgtests: py3versions: use -s instead of -r

 -- Agathe Porte <debian@microjoe.org>  Fri, 02 Dec 2022 14:19:53 +0100

dt-schema (2022.08.2-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 18:28:21 +0100

dt-schema (2022.08.2-2) unstable; urgency=medium

  * Team upload
  * Add B-D setuptools (Closes: #1020021)

 -- Bastian Germann <bage@debian.org>  Tue, 20 Sep 2022 12:57:44 +0200

dt-schema (2022.08.2-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2022.08.2
  * Build only for default python3
  * Add explicit libfdt (not found by pybuild)

 -- Bastian Germann <bage@debian.org>  Mon, 22 Aug 2022 01:58:31 +0200

dt-schema (2022.07-1) unstable; urgency=medium

  * Team upload
  * d/salsa-ci.yml: Replace with central CI config
  * New upstream version 2022.07
  * d/copyright: Update with new version's info

 -- Bastian Germann <bage@debian.org>  Mon, 18 Jul 2022 17:43:17 +0200

dt-schema (2021.10-2) unstable; urgency=medium

  * d/copyright: mark dual-licensed files

 -- Agathe Porte <debian@microjoe.org>  Mon, 06 Jun 2022 10:36:42 +0200

dt-schema (2021.10-1) unstable; urgency=low

  * Initial release; Closes: #1005301

 -- Agathe Porte <debian@microjoe.org>  Thu, 10 Feb 2022 20:44:24 +0000
